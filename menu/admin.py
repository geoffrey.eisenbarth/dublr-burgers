from django.contrib import admin

from menu.models import MenuCategory, MenuItem


@admin.register(MenuCategory)
class MenuCategoryAdmin(admin.ModelAdmin):
  pass

@admin.register(MenuItem)
class MenuItemAdmin(admin.ModelAdmin):
  list_display = ('name', 'price', 'category')
