from __future__ import unicode_literals

from django.db import models


class MenuCategory(models.Model):
  name  = models.CharField(max_length=32)
  order = models.IntegerField(default=0, help_text='The order is the order that this category should appear when rendered in the menu.')

  class Meta:
    verbose_name = 'Menu Category'
    verbose_name_plural = 'Menu Categories'
    ordering = ['order', 'name']

  def __str__(self):
    return self.name


class MenuItem(models.Model):
  name = models.CharField(max_length=48)
  description = models.CharField(max_length=128, null=True, blank=True)
  category = models.ForeignKey(MenuCategory, null=True, blank=True)
  order = models.IntegerField(default=0, help_text='Specify the order this appears in the menu.')
  price = models.DecimalField(max_digits=8, decimal_places=2)
  image = models.ImageField(upload_to='menu', null=True, blank=True)

  class Meta:
    verbose_name = 'Menu Item'
    verbose_name_plural = 'Menu Items'
    #ordering = ['category', 'order', 'name']

  def __str__(self):
    return self.name
